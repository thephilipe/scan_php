# This script was developed for self test and learning Python and is for educational purposes only!

The script makes a GET call to the given target website and then examines the X-Powered-By head of the response header to determine the PHP version that was delivered.

The website is utilizing an old version of PHP that contains known vulnerabilities if the PHP version is 5.6 or below. The email recipient is then informed about the vulnerability in a notification email.

This script is only a simple illustration; take aware that if the recipient's email server is blocking the message or if the credentials are wrong, sending an email using SMTP may not function as planned.